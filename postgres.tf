resource "helm_release" "postgres" {
  name       = "postgresql"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "postgresql"
  version    = "12.2.8"

  values = [
    "${file("postgres-values.yaml")}"
  ]
}
